<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Grupos de botones</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
<div class="container">
    <br>
    <div class="btn-toolbar ">
      <div class="btn-group btn-group-lg">
        <button class="btn btn-default">Centro</button>
        <button class="btn btn-default">Derecha</button>
        <button class="btn btn-default">Izquierda</button>
      </div>
      <div class="btn-group">
        <button class="btn btn-default">1</button>
        <button class="btn btn-default">2</button>
        <button class="btn btn-default">3</button>
      </div>
      <div class="btn-group btn-group-xs">
        <button class="btn btn-default">Centro</button>
        <button class="btn btn-default">Derecha</button>
        <button class="btn btn-default">Izquierda</button>
      </div>
      <div class="btn-group btn-group-sm">
        <button class="btn btn-default">Centro</button>
        <button class="btn btn-default">Derecha</button>
        <button class="btn btn-default">Izquierda</button>
      </div>
    </div>
    <br>
    
    <div class="btn-group-vertical">
      <button class="btn btn-default">Centro</button>
      <button class="btn btn-default">Derecha</button>
      <button class="btn btn-default">Izquierda</button>
      <!--Le agregamos un dropdown-->
      
      <div class="btn-group">
        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" >
          Dropdown <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Dropdown link</a></li>
          <li><a href="#">Dropdown link</a></li>
        </ul></div>
    </div>
  </div>
  <br>
  <br>
  <!--Justificado con enlaces-->
  <div class="btn-group btn-group-justified">
    <a href="#" class="btn btn-default">1</a>
    <a href="#" class="btn btn-default">2</a>
    <a href="#" class="btn btn-default">3</a>
  </div>
  <!--Justficado con botones. Para esto a cada uno lo tenemos que encerrar en un grupo-->
  <div class="btn-group btn-group-justified">
    <div class="btn-group">
      <button class="btn btn-default">contenido</button>
    </div>
    <div class="btn-group">
      <button class="btn btn-default">contenido</button>
    </div>
    <div class="btn-group">
      <button class="btn btn-default">contenido</button>
    </div>
  </div>
  <script src="../js/jquery-latest.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</body>
</html>