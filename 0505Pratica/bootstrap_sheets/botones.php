<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Botones</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-3">
        <button class="btn btn-default">Default</button>
        <button class="btn btn-primary" disabled>Primary</button>
        <button class="btn btn-success">Success</button>
        <button class="btn btn-info">Info</button>
        <button class="btn btn-warning">Warning</button>
        <button class="btn btn-danger">Danger</button>
        <button class="btn btn-link">Link</button>
      </div>

      <div class="col-md-3">
        <a href="" class="btn btn-xs btn-default">Default</a>
        <a href="" class="btn btn-xs btn-primary" disabled>Primary</a>
        <a href="" class="btn btn-xs btn-success">Success</a>
        <a href="" class="btn btn-xs btn-info">Info</a>
        <a href="" class="btn btn-xs btn-warning">Warning</a>
        <a href="" class="btn btn-xs btn-danger">Danger</a>
        <a href="" class="btn btn-xs btn-link">Link</a>
      </div>
      <div class="col-md-3">
        <button class="btn btn-block active btn-default">Default</button>
        <button class="btn btn-block active btn-primary">Primary</button>
        <button class="btn btn-block active btn-success">Success</button>
        <button class="btn btn-block active btn-info" disabled>Info</button>
        <button class="btn btn-block active btn-warning">Warning</button>
        <button class="btn btn-block active btn-danger">Danger</button>
        <button class="btn btn-block active btn-link">Link</button>
      </div>

      <div class="col-md-3">
        <a href="" class="btn btn-block btn-default">Default</a>
        <a href="" class="btn btn-block btn-primary">Primary</a>
        <a href="" class="btn btn-block btn-success">Success</a>
        <a href="" class="btn btn-block btn-info" disabled>Info</a>
        <a href="" class="btn btn-block btn-warning">Warning</a>
        <a href="" class="btn btn-block btn-danger">Danger</a>
        <a href="" class="btn btn-block btn-link">Link</a>
      </div>
    </div>
  </div>
  
  <script src="../js/jquery-latest.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</body>
</html>