<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Navs</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>

  <div class="container-fluid">
   
    <br>
    <div class="row">
      <div class="col-md-4">
        <ul class="nav nav-tabs">
          <li role="presentation" class="active"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
        </ul>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
        </ul>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <ul class="nav nav-pills nav-stacked">
          <li role="presentation" class="active"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
        </ul>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <ul class="nav nav-tabs nav-justified">
          <li role="presentation" class="active"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
        </ul>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-4">
        <ul class="nav nav-pills nav-justified">
          <li role="presentation" class="active"><a href="#">Contenido</a></li>
          <li role="presentation" class="disabled"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
        </ul>
      </div>
    </div>
    <br>
    <div class="row">
      <div class="col-md-5">
        <ul class="nav nav-tabs">
          <li role="presentation" class="active"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
          <li role="presentation"><a href="#">Contenido</a></li>
          <li role="presentation" class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" >
              Dropdown <span class="caret"></span>
            </a>
            <ul class="dropdown-menu">
              <li role="presentation" class="active"><a href="#">Contenido</a></li>
              <li role="presentation"><a href="#">Contenido</a></li>
              <li role="presentation"><a href="#">Contenido</a></li> 
            </ul>
          </li> 
        </ul>
      </div>
    </div>
  </div>
  <script src="../js/jquery-latest.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</body>
</html>