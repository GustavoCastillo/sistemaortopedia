<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>NavBars</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
  <div class="container-fluid">
    <header>
      <!--Si queremos solo le dejamos default, navbar-default, o podemos agregar: navbar-fixed-top o navbar-fixed-botton, navbar-static-top, navbar-inverse -->
      <nav class="navbar navbar-fixed-top navbar-inverse">
       
        <!--Comenzamos por poner el contenedor para el contenido del navbar-->
        <div class="container-fluid">
         
          <!--Ahora ponemos el boton pequeño para cuando la resolucion es muy baja-->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-1">
             
              <!--Este es un tag para los dispositivos de solo lectura-->
              <span class="sr-only">Menu</span>
              
              <!--Por cada span se visualiza una linea en el icono-->
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            
            <!--Esto es lo que aparece como nombre de la barra con un enlace-->
            <a href="#" class="navbar-brand">Nombre encabezado</a>
          </div>
          
          <!--En este caso el ID apunta al atributo DATA-TARGET del boton anterior-->
          <div class="collapse navbar-collapse" id="navbar-1">
           
            <!--Aca agrego los tres items que se ven en el menu no colapsado-->
            <ul class="nav navbar-nav">
              <li><a href="#">Item#1</a></li>
              
              <!--A este lo ponemos como activo-->
              <li class="active"><a href="#">Item#2</a></li>
              
              <!--A este le vamos a agregar un submenu le agregamos la clase dropdown-->
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                  Dropdown <span class="caret"></span>
                </a>
                <ul class="dropdown-menu">
                  <li><a href="#">Item #1</a></li>
                  <li><a href="#">Item #2</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Item #4</a></li>
                </ul>
              </li>
            </ul>
            
            <!--Agrego un buscador-->
            <form action="" class="navbar-form navbar-left" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Buscar">
              </div>
            </form>
            
            <!--Agrego un nuevo menu de navegacion a la derecha-->
            <div class="nav navbar-nav navbar-right">
             
              <!--Y le agrego los mismos items que en el menu anterior-->
              <ul class="nav navbar-nav">
                <li><a href="#">Item#1</a></li>
                
                <!--A este lo ponemos como activo-->
                <li class="active"><a href="#">Item#2</a></li>
                
                <!--A este le vamos a agregar un submenu le agregamos la clase dropdown-->
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                    Dropdown <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                    <li><a href="#">Item #1</a></li>
                    <li><a href="#">Item #2</a></li>
                    <li class="divider"></li>
                    <li><a href="#">Item #4</a></li>
                  </ul>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </nav>
    </header>
  </div>
  
  <script src="../js/jquery-latest.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</body>
</html>