<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>DropDown</title>
  <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
  <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
  <div class="container fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="button-group">
          <button class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            Accion <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Items 1</a></li>
            <li><a href="#">Items 2</a></li>
            <li><a href="#">Items 3</a></li>
            <li><a href="#">Items 4</a></li>
            <li><a href="#">Items 5</a></li>
          </ul>
        </div>
        <div class="button-group">
          <button type="button" class="btn btn-default">Texto</button>
          <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
            <span class="caret"></span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Items 1</a></li>
            <li><a href="#">Items 2</a></li>
            <li><a href="#">Items 3</a></li>
            <li><a href="#">Items 4</a></li>
            <li><a href="#">Items 5</a></li>
          </ul>
        </div>
      </div>
      <div class="btn-group">
        <button type="button" class="btn btn-danger">Título del botón</button>

        <button type="button" class="btn btn-danger dropdown-toggle"
                data-toggle="dropdown">
          <span class="caret"></span>
          <span class="sr-only">Desplegar menú</span>
        </button>

        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Acción #1</a></li>
          <li><a href="#">Acción #2</a></li>
          <li><a href="#">Acción #3</a></li>
          <li class="divider"></li>
          <li><a href="#">Acción #4</a></li>
        </ul>
      </div>
    </div>
  </div>

  <script src="../js/jquery-latest.js"></script>
  <script src="../js/bootstrap.min.js"></script>
</body>
</html>